package com.tricon.graphqlspqr.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tricon.graphqlspqr.query.AuthorQuery;
import com.tricon.graphqlspqr.query.BookQuery;

import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.SchemaPrinter;
import io.leangen.graphql.GraphQLSchemaGenerator;
import io.leangen.graphql.metadata.strategy.query.AnnotatedResolverBuilder;
import io.leangen.graphql.metadata.strategy.query.PublicResolverBuilder;
import io.leangen.graphql.metadata.strategy.value.jackson.JacksonValueMapperFactory;

@RestController
public class GraphqlController {
	
	
	
	  private final GraphQL graphQL;

	    @Autowired
	    public GraphqlController(BookQuery bookQuery,AuthorQuery authorQuery) {

	        //Schema generated from query classes
	        GraphQLSchema schema = new GraphQLSchemaGenerator()
	                .withResolverBuilders(
	                        //Resolve by annotations
	                        new AnnotatedResolverBuilder(),
	                        //Resolve public methods inside root package
	                        new PublicResolverBuilder("com.tricon.graphqlspqr"))
	                .withOperationsFromSingleton(bookQuery)
	                .withOperationsFromSingleton(authorQuery)
	                .withValueMapperFactory(new JacksonValueMapperFactory())
	                .generate();
	        graphQL = GraphQL.newGraphQL(schema).build();
	        
	        System.out.println(new SchemaPrinter().print(schema));
	    }
	
	@PostMapping(value="/graphql")
	public Map<String, Object> executeQuery(@RequestBody Map<String, String> request){
		ExecutionResult result =  graphQL.execute(request.get("query"));
		return result.getData();
	}

}
