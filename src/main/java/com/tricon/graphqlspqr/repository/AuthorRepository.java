package com.tricon.graphqlspqr.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tricon.graphqlspqr.vo.Author;

public interface AuthorRepository extends MongoRepository<Author, Integer> {

}
