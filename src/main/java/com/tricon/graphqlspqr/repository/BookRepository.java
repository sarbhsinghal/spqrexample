package com.tricon.graphqlspqr.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tricon.graphqlspqr.vo.Book;

public interface BookRepository extends MongoRepository<Book, Integer>{
	
	public List<Book> findAllByAuthorId(int authorId);

}
