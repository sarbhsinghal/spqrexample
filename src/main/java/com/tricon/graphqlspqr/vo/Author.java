package com.tricon.graphqlspqr.vo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Author")
public class Author {
	
	@Id
	private int authorId;
	private String authorName;
	
	public Author(int authorId,String authorName) {
		super();
		this.authorId = authorId;
		this.authorName = authorName;
	}
	public Author() {
		super();
	}
	public int getAuthorId() {
		return authorId;
	}
	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", authorName=" + authorName + "]";
	}
	
	

}
