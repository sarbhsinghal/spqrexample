package com.tricon.graphqlspqr.vo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Book")
public class Book {
	
	@Id
	private int isn;
	private String title;
	private String publisher;
	private int authorId;
	
	public Book(int isn,String title, int authorId, String publisher) {
		super();
		this.isn=isn;
		this.title = title;
		this.authorId = authorId;
		this.publisher = publisher;
	}

	public Book() {
		super();
	}

	public int getIsn() {
		return isn;
	}

	public void setIsn(int isn) {
		this.isn = isn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	@Override
	public String toString() {
		return "Book [isn=" + isn + ", title=" + title + ", authorId=" + authorId + ", publisher=" + publisher + "]";
	}
	
	
	
}
