package com.tricon.graphqlspqr.query;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tricon.graphqlspqr.repository.AuthorRepository;
import com.tricon.graphqlspqr.repository.BookRepository;
import com.tricon.graphqlspqr.vo.Author;
import com.tricon.graphqlspqr.vo.Book;

import io.leangen.graphql.annotations.GraphQLContext;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;

@Component
public class BookQuery {

	@Autowired
	BookRepository bookRepository;

	@Autowired
	AuthorRepository authorRepository;

	@GraphQLQuery
	public List<Book> allBooks() {
		return bookRepository.findAll();
	}

	@GraphQLQuery
	public Optional<Book> book(int isn) {
		return bookRepository.findById(isn);
	}

	@GraphQLMutation
	public Book createBook(int isn, String title, int authorId, String publisher) {
		return bookRepository.insert(new Book(isn, title, authorId, publisher));
	}

	@GraphQLQuery
	public Optional<Author> authorBy(@GraphQLContext Book book) {
		return authorRepository.findById(book.getAuthorId());
	}

}
