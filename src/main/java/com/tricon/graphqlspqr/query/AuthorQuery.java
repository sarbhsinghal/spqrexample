package com.tricon.graphqlspqr.query;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tricon.graphqlspqr.repository.AuthorRepository;
import com.tricon.graphqlspqr.repository.BookRepository;
import com.tricon.graphqlspqr.vo.Author;
import com.tricon.graphqlspqr.vo.Book;

import io.leangen.graphql.annotations.GraphQLContext;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;

@Component
public class AuthorQuery {
	
	@Autowired
	AuthorRepository authorRepository;
	
	@Autowired
	BookRepository bookRepository;
	
	@GraphQLMutation
	public Author createAuthor(int authorId,String authorName) {
		return authorRepository.insert(new Author(authorId,authorName));
	}
	
	@GraphQLQuery
	public List<Author> getAllAuthor(){
		return authorRepository.findAll();
	}
	
	@GraphQLQuery
	public Optional<Author> author(int authorId){
		return authorRepository.findById(authorId);
	}
	
	@GraphQLQuery
	public List<Book> getBooksByAuthor(@GraphQLContext Author author){
		return bookRepository.findAllByAuthorId(author.getAuthorId());
	}

}
